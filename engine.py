import sys
import pygame

class Game:

    def __init__(self, title, size, color):

        pygame.init()

        self.screen = pygame.display.set_mode((size))

        pygame.display.set_caption(f"{title}")

        self.bg_colour = color
    
    def run(self):

        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
            
            self.screen.fill(self.bg_colour)
            
            pygame.display.flip()
